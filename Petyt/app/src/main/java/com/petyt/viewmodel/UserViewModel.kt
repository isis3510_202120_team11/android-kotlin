package com.petyt.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.GeoPoint
import com.petyt.contracts.UserContract
import com.petyt.domain.UserUseCase
import com.petyt.models.UserModel
import kotlinx.coroutines.launch

class UserViewModel() : ViewModel() {

    val userModel = MutableLiveData<UserModel>()
    val results = MutableLiveData<Boolean>()
    val location = MutableLiveData<GeoPoint>()
    val userUseCase = UserUseCase()

    fun onCreate() {
        viewModelScope.launch {
            userModel.value = UserModel()
        }
    }
    fun login(email: String, password: String){
        userUseCase.loginUseCase(email, password).addOnSuccessListener { user ->
            results.value = !user.isEmpty
        }
    }
    fun register(user: UserModel){
        userUseCase.registerUser(user)
            .addOnSuccessListener { results.value = true }
            .addOnFailureListener { results.value = false }
    }
    fun getUser(user: String){
        userUseCase.getUser(user).addOnSuccessListener { user ->
            location.value = user.documents[0].getGeoPoint(UserContract.C_LOCATION)
        }
    }
}