package com.petyt.utils

import android.graphics.Bitmap
import android.util.Base64
import java.io.ByteArrayOutputStream
import android.graphics.BitmapFactory

class Base64Converter {
     fun getBase64Image(image: Bitmap): String {
        val outputStream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 60, outputStream)
        val biteArray = outputStream.toByteArray()
        return Base64.encodeToString(biteArray, Base64.DEFAULT)
    }

    fun getBitmapImage(base: String): Bitmap{
        val decodedString: ByteArray = Base64.decode(base, Base64.DEFAULT)
        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        return decodedByte
    }
}
