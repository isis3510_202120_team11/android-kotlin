package com.petyt.ui.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.petyt.R
import com.petyt.adapters.RvAdapterAppoints
import com.petyt.utils.SharedPreferences
import com.petyt.interfaces.ClickListenerPet
import com.petyt.models.AppointModel
import com.petyt.utils.AppointLocal
import com.petyt.utils.ConectivityCheck
import com.petyt.viewmodel.AppointViewModel

class AgendaActivity : AppCompatActivity() {

    lateinit var rvWalkers: RecyclerView
    lateinit var adapterLista: RvAdapterAppoints
    lateinit var layoutMan: RecyclerView.LayoutManager
    lateinit var btnHome: Button
    lateinit var appointLocal: AppointLocal
    val appointViewModel: AppointViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agenda)
        
        rvWalkers = findViewById(R.id.rvAppoint)
        btnHome = findViewById(R.id.btn_home)
        rvWalkers.setHasFixedSize(true)
        appointLocal = AppointLocal(this)
        layoutMan = LinearLayoutManager(this)
        appointViewModel.listAppoint.observe(this, { setupRecycler(it) })

        if(ConectivityCheck.isConnected(this)){
            appointViewModel.getAppoints(SharedPreferences(this).getOwner())
        }
        else{
            val appoints = appointLocal.getAppo()
            appointViewModel.listAppoint.value = appoints
            Toast.makeText(this, "Sin conexión a internet", Toast.LENGTH_SHORT).show()
        }
        btnHome.setOnClickListener { finish() }
    }

    fun setupRecycler(appoints: ArrayList<AppointModel>){
        adapterLista = RvAdapterAppoints(appoints, object : ClickListenerPet {
            override fun clickPet(vista: View, index: Int) {
                Toast.makeText(this@AgendaActivity, "fecha agendada", Toast.LENGTH_SHORT).show()
            }
        })
        rvWalkers.layoutManager = layoutMan
        rvWalkers.adapter = adapterLista
    }
}