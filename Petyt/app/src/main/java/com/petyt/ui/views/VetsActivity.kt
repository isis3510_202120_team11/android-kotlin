package com.petyt.ui.views

import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.petyt.adapters.RvAdapterVets
import com.petyt.viewmodel.VetViewModel
import com.petyt.R
import com.petyt.interfaces.ClickListenerVet
import com.petyt.models.VetModel
import com.petyt.utils.SharedPreferences
import com.petyt.utils.VetsFilter

class VetsActivity: AppCompatActivity() {

    lateinit var btnHome: Button
    lateinit var btnAgenda: Button
    lateinit var btnSearch: Button
    lateinit var fldSearch: EditText
    lateinit var adapterList: RvAdapterVets
    lateinit var rvVets: RecyclerView

    lateinit var layoutManager: RecyclerView.LayoutManager
    val vetViewModel: VetViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vets)
        vetViewModel.onCreate()
        rvVets = findViewById(R.id.rv_vets)
        btnHome = findViewById(R.id.btn_home_vets)
        btnAgenda = findViewById(R.id.btn_agenda_vets)
        btnSearch = findViewById(R.id.btn_search_vets)
        rvVets.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        btnSearch.setOnClickListener{ searchVet() }
        btnHome.setOnClickListener{ finish()}
        btnAgenda.setOnClickListener{
            //Create agenda service
            startActivity(Intent(this, VetAgendaActivity:: class.java))
            finish()
        }

        vetViewModel.locate.observe(this, { vetViewModel.getVetList() })
        vetViewModel.listVets.observe(this, { setupRecycler(it) })
        getLocation()

    }

    fun getLocation(){
        val geoLocation = SharedPreferences(this).getLocation()
        val location = Location("")
        location.latitude = geoLocation.latitude
        location.longitude = geoLocation.longitude
        vetViewModel.locate.value = location
    }

    fun setupRecycler(vets: ArrayList<VetModel>){
        val filterVet = VetsFilter().vetsByLocation(vets, vetViewModel.locate.value!!)
        adapterList = RvAdapterVets(
            vetsList= filterVet,
            clickListen = object : ClickListenerVet {
                override fun clickVet(vista: View, index: Int) {
                    val intent =  Intent(this@VetsActivity, AddVetAppointActivity::class.java)
                    intent.putExtra("vets", vets[index].profile)
                    startActivity(intent)
                }


            }
        )
        rvVets.layoutManager = layoutManager
        rvVets.adapter = adapterList
    }

    fun searchVet(){
        val vet = fldSearch.text.toString()
        if(!vet.isNullOrEmpty()){
            vetViewModel.searchVet(vet)
        }
        else{
            vetViewModel.getVetList()
        }
    }

}