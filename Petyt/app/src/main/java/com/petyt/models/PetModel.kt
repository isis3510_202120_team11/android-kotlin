package com.petyt.models

data class PetModel(
    val name: String = "",
    val breed: String = "",
    val age: Number = 0,
    val owner: String = "",
    val sex: String = "",
    val birth: String = "",
    val imageStr: String = "" ,
    val humor: String = "",
    val size: String = "",
    val id: String = ""
)