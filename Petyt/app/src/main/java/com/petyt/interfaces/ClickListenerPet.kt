package com.petyt.interfaces

import android.view.View

interface ClickListenerPet {
    fun clickPet(vista: View, index: Int)
}