package com.petyt.ui.views

import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import com.petyt.R
import android.widget.Button
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.petyt.adapters.RvAdapterPets
import com.petyt.adapters.RvAdapterVetAppoint
import com.petyt.interfaces.ClickListenerPet
import com.petyt.models.VetAppointModel
import com.petyt.utils.ConectivityCheck
import com.petyt.utils.SharedPreferences
import com.petyt.utils.VetAppointLocal
import com.petyt.viewmodel.VetAppointViewModel

class VetAgendaActivity: AppCompatActivity() {

    lateinit var rvVets: RecyclerView
    lateinit var adapterList: RvAdapterVetAppoint
    lateinit var  layoutManager: RecyclerView.LayoutManager
    lateinit var btnHome: Button
    lateinit var vetAppointLocal: VetAppointLocal
    val vetAppointViewModel: VetAppointViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vet_agenda)
        rvVets = findViewById((R.id.rvVetAppoint))
        btnHome = findViewById(R.id.btn_home)
        rvVets.setHasFixedSize(true)
        vetAppointLocal = VetAppointLocal(this)
        layoutManager = LinearLayoutManager(this)

        vetAppointViewModel.listVetAppoint.observe(this, { setupRecycler(it)})

        if(ConectivityCheck.isConnected(this)){
            vetAppointViewModel.getVetAppoints(SharedPreferences(this).getOwner())
        }
        else{
            val appoints = vetAppointLocal.getVetAppoint()
            vetAppointViewModel.listVetAppoint.value = appoints
            Toast.makeText(this, "Sin conexión a internet", Toast.LENGTH_SHORT).show()
        }
        btnHome.setOnClickListener { finish() }
    }

    fun setupRecycler(vetAppoint: ArrayList<VetAppointModel>){
        adapterList = RvAdapterVetAppoint(vetAppoint, object : ClickListenerPet{
            override fun clickPet(vista: View, index: Int) {
                Toast.makeText(this@VetAgendaActivity, "fecha agendada", Toast.LENGTH_SHORT).show()
            }
        })
        rvVets.layoutManager = layoutManager
        rvVets.adapter = adapterList
    }

}