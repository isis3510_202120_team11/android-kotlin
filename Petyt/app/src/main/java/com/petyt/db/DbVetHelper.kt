package com.petyt.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.petyt.contracts.VetAppointContract

class DbVetHelper (context: Context): SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    val contract = VetAppointContract

    private val CREATE_TB_VET_APPOINT = "CREATE TABLE ${contract.N_TABLA} " +
            "(${contract.C_ID} INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "${contract.C_FECHA} TEXT, ${contract.C_HORA} TEXT," +
            "${contract.C_CLIENT} TEXT, ${contract.C_PROVIDER} TEXT,"+
            "${contract.C_PET} TEXT, ${contract.C_PRICE} TEXT)"

    private val REMOVE_TB_VET_APPOINT = "DROP TABLE IF EXISTS ${contract.N_TABLA}"

    override fun onCreate(db: SQLiteDatabase?){
        with(db!!){
            execSQL(CREATE_TB_VET_APPOINT)
        }
    }
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        with(db!!){
            execSQL(REMOVE_TB_VET_APPOINT)
        }
        onCreate(db)
    }

    companion object{
        val DB_NAME = "vets_appoint.db"
        val DB_VERSION = 1
    }



}