package com.petyt.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.petyt.R
import com.petyt.interfaces.ClickListenerVet
import com.petyt.models.VetModel
import com.squareup.picasso.Picasso
import java.text.DecimalFormat

class RvAdapterVets (var vetsList: ArrayList<VetModel>,
                     var clickListen: ClickListenerVet): RecyclerView.Adapter<RvAdapterVets.viewHold>() {

    var vets = vetsList
    lateinit var viewHolder: viewHold

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHold {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.vet_view,parent, false)
        viewHolder = viewHold(view, clickListen)
        return viewHolder
    }

    override fun onBindViewHolder(holder: viewHold, position: Int) {
        val vet = vets[position]
        holder.name.text = vet.name
        val rate = "$${vet.rate}"
        holder.price.text = rate
        holder.rating.rating = vet.rating.toFloat()
        Picasso.get().load(vet.image).into(holder.image)
        if(vet.distance !=0.0F){
            val format = DecimalFormat("#.0")
            val distanceText =  "a ${format.format(vet.distance/1000)} km de tí"
            holder.distance.text =distanceText
        }
        holder.specialty.text = vet.specialty
    }

    override fun getItemCount(): Int {
        return vets.size
    }

    class viewHold(var vista: View, listen: ClickListenerVet): RecyclerView.ViewHolder(vista), View.OnClickListener {
        var name: TextView //crear una variable para cada elemento de la vista
        var price: TextView
        var rating: RatingBar
        var image: ImageView
        var distance: TextView
        var specialty: TextView
        var listener: ClickListenerVet

        init {
            this.listener = listen
            name = vista.findViewById(R.id.vv_name_vet) //asociamos cada variable con la vista
            price = vista.findViewById(R.id.vv_price_vet)
            rating = vista.findViewById(R.id.rb_rating_vet)
            image = vista.findViewById(R.id.iv_photo_vet)
            distance = vista.findViewById(R.id.vv_distance_vet)
            specialty = vista.findViewById(R.id.vv_specialty_vet)
            vista.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            this.listener.clickVet(v!!, adapterPosition)
        }
    }



}