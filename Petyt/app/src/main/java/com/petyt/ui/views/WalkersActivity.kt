package com.petyt.ui.views


import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.GeoPoint
import com.petyt.R
import com.petyt.adapters.RvAdapterWalkers
import com.petyt.interfaces.ClickListenerWalker
import com.petyt.models.WalkerModel
import com.petyt.utils.LocationUtil
import com.petyt.utils.SharedPreferences
import com.petyt.utils.WalkersFilter
import com.petyt.viewmodel.UserViewModel
import com.petyt.viewmodel.WalkerViewModel

class WalkersActivity : AppCompatActivity() {

    lateinit var btnHome: Button
    lateinit var btnAgenda: Button
    lateinit var btnRecom: Button
    lateinit var btnSearch: Button
    lateinit var etSearch: EditText
    lateinit var rvWalkers: RecyclerView
    lateinit var adapterLista: RvAdapterWalkers
    lateinit var layoutMan: RecyclerView.LayoutManager
    val walkerViewModel: WalkerViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_walkers)
        walkerViewModel.onCreate()
        rvWalkers = findViewById(R.id.rv_walkers)
        btnHome = findViewById(R.id.btn_home_walk)
        btnAgenda = findViewById(R.id.btn_agenda_walk)
        btnRecom = findViewById(R.id.btn_recom_walkers)
        btnSearch = findViewById(R.id.btn_search_walkers)
        etSearch = findViewById(R.id.et_search_walkers)
        rvWalkers.setHasFixedSize(true)
        layoutMan = LinearLayoutManager(this)
        btnSearch.setOnClickListener { searchWalker() }
        btnHome.setOnClickListener { finish() }
        btnRecom.setOnClickListener { startActivity(Intent(this, WalkCompatActivity::class.java)) }
        btnAgenda.setOnClickListener {
            startActivity(Intent(this, AgendaActivity::class.java))
            finish()
        }

        walkerViewModel.locate.observe(this, {walkerViewModel.getWalkerList()}) //observar cuando hay un cambio (camara de la puerta)
        walkerViewModel.listWalkers.observe(this, { setupRecycler(it) })
        getLocation()
    }
    fun getLocation(){
        val geoLocation = SharedPreferences(this).getLocation()
        val location = Location("")
        location.latitude = geoLocation.latitude
        location.longitude = geoLocation.longitude
        walkerViewModel.locate.value = location
    }
    fun setupRecycler(walkers: ArrayList<WalkerModel>){
        val filterWalker = WalkersFilter().byLocation(walkers, walkerViewModel.locate.value!!)
        adapterLista = RvAdapterWalkers(
            walkersList= filterWalker,
            clickListen = object : ClickListenerWalker {
                override fun clickWalk(vista: View, index: Int) {
                    val intent = Intent(this@WalkersActivity, AddApointActivity::class.java)
                    intent.putExtra("walker", walkers[index].profile)
                    startActivity(intent)
                }
            })
        rvWalkers.layoutManager = layoutMan
        rvWalkers.adapter = adapterLista
    }
    fun searchWalker(){
        val walker = etSearch.text.toString()
        if(!walker.isNullOrEmpty()){
            walkerViewModel.searchWalker(walker)
        }
        else walkerViewModel.getWalkerList()
    }
}