package com.petyt.models

import com.google.firebase.firestore.GeoPoint

data class WalkerModel(
    val name: String = "",
    val profile: String = "",
    val price: Number = 0,
    val rating: Number = 0,
    val image:String = "",
    val location: GeoPoint = GeoPoint(0.0, 0.0),
    val id: String = "",
    val sinergia: ArrayList<String> = ArrayList(),
    val compat: Int = 0,
    var distance: Float = 0.0F
)