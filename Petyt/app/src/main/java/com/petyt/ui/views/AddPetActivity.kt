package com.petyt.ui.views

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.*
import androidx.activity.viewModels
import com.petyt.R
import com.petyt.utils.Base64Converter
import com.petyt.utils.SharedPreferences
import com.petyt.utils.galeriaUtil
import com.petyt.models.PetModel
import com.petyt.viewmodel.PetViewModel

class AddPetActivity : AppCompatActivity() {

    lateinit var etPetName: EditText
    lateinit var etPetAge: EditText
    lateinit var etPetBreed: EditText
    lateinit var etPetSex: EditText
    lateinit var btnAddPet: Button
    lateinit var btnAddPhoto: Button
    lateinit var btnAddPhotoAlbum: Button
    lateinit var ivPhoto: ImageView
    lateinit var spDay: Spinner
    lateinit var spYear: Spinner
    lateinit var spMonth: Spinner
    lateinit var spTemperament: Spinner
    lateinit var spSize: Spinner
    val REQUEST_IMAGE_CAPTURE = 1
    val REQUEST_GALLERY = 110
    lateinit var imageUri: Uri
    val petViewModel: PetViewModel by viewModels()
    lateinit var image: Bitmap
    private var imageStr: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_pet)
        etPetName = findViewById(R.id.et_pet_name)
        etPetAge = findViewById(R.id.et_pet_age)
        etPetBreed = findViewById(R.id.et_pet_breed)
        etPetSex = findViewById(R.id.et_pet_sex)
        btnAddPet = findViewById(R.id.btn_add_pet)
        btnAddPhotoAlbum = findViewById(R.id.btn_add_photo)
        btnAddPhoto = findViewById(R.id.btn_take_photo)
        ivPhoto = findViewById(R.id.iv_pet_photo_prev)
        spDay = findViewById(R.id.sp_pet_day)
        spMonth = findViewById(R.id.sp_pet_month)
        spYear = findViewById(R.id.sp_pet_year)
        spTemperament = findViewById(R.id.sp_pet_temp)
        spSize = findViewById(R.id.sp_pet_size)
        btnAddPet.setOnClickListener { addPet() }
        btnAddPhoto.setOnClickListener { takePhoto() }
        btnAddPhotoAlbum.setOnClickListener { selectPhoto() }
    }

    fun addPet() {
        val name = etPetName.text
        val age = etPetAge.text
        val breed = etPetBreed.text
        val sex = etPetSex.text
        val birth = "${spDay.selectedItem}/${spMonth.selectedItem}/${spYear.selectedItem}"
        val owner = SharedPreferences(this).getOwner()
        if(name.isEmpty()) return Toast.makeText(this, "Debes ingresar el nombre", Toast.LENGTH_SHORT).show()
        if(age.isEmpty()) return Toast.makeText(this, "Debes ingresar la edad", Toast.LENGTH_SHORT).show()
        if(breed.isEmpty()) return Toast.makeText(this, "Debes ingresar la raza", Toast.LENGTH_SHORT).show()
        if(sex.isEmpty()) return Toast.makeText(this, "Debes ingresar el sexo", Toast.LENGTH_SHORT).show()
        val pet = PetModel(name.toString(), breed.toString(), age.toString().toInt(), owner, sex.toString(), birth, imageStr,
            size = spSize.selectedItem.toString(), )
        petViewModel.onCreate(owner)
        petViewModel.addPet(pet)
        petViewModel.result.observe(this, {
            if(it){
                Toast.makeText(this, "Mascota Agregada", Toast.LENGTH_SHORT).show()
                finish()
            }
        })
    }

    @SuppressLint("QueryPermissionsNeeded")
    private fun takePhoto() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }
    fun selectPhoto(){
        galeriaUtil().seleccionarFoto(this, REQUEST_GALLERY)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            data?.extras?.let { bundle ->
                image = bundle.get("data") as Bitmap
                ivPhoto.setImageBitmap(image)
                imageStr = Base64Converter().getBase64Image(image)
            }
        }
        if(requestCode == REQUEST_GALLERY && resultCode == RESULT_OK){
            imageUri = data!!.data!!
            ivPhoto.setImageURI(imageUri)
            val bitmap = (ivPhoto.drawable as BitmapDrawable).bitmap
            imageStr = Base64Converter().getBase64Image(bitmap)
        }
    }
}