package com.petyt.data

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.petyt.contracts.WalkerContract

class WalkerData {
    private val db = FirebaseFirestore.getInstance()
    private val contract = WalkerContract
    private var walkerCollection = db.collection(contract.N_TABLA)

    fun getWalkers(): Task<QuerySnapshot> {
        return walkerCollection.get()
    }


    fun obtWalker(mail:String): Task<QuerySnapshot> {
        return walkerCollection.whereEqualTo(contract.C_PROFILE, mail).get()
    }


    fun obtBestWalkers(): Task<QuerySnapshot> {
        return walkerCollection.whereGreaterThan(contract.C_RATING, 3.5).get()
    }

    fun searchWalkers(walkerName: String): Task<QuerySnapshot>{
        return walkerCollection.whereEqualTo(contract.C_NAME, walkerName).get()
    }
}