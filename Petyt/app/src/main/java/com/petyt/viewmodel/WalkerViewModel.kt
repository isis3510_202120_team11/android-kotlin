package com.petyt.viewmodel

import android.location.Location
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.getField
import com.petyt.contracts.WalkerContract
import com.petyt.domain.WalkerUseCase
import com.petyt.models.WalkerModel
import kotlinx.coroutines.launch

class WalkerViewModel: ViewModel() {
    val listWalkers = MutableLiveData<ArrayList<WalkerModel>>()
    val walkerData = MutableLiveData<WalkerModel>()
    val locate = MutableLiveData<Location>()
    private val useCase = WalkerUseCase()

    fun onCreate() {
        viewModelScope.launch {
            locate.value = Location("")

        }
    }
    fun getWalkerList(){
        useCase.getWalkers().addOnSuccessListener { walkersdb ->
            val walkers = ArrayList<WalkerModel>()
            for (walk in walkersdb) {
                walkers.add(
                    WalkerModel(name = walk.get(WalkerContract.C_NAME) as String,
                        price = walk.get(WalkerContract.C_PRICE) as Number,
                        rating = walk.get(WalkerContract.C_RATING) as Number,
                        profile = walk.get(WalkerContract.C_PROFILE) as String,
                        image = walk.get(WalkerContract.C_IMG) as String,
                        location = walk.getGeoPoint("location") as GeoPoint,
                        sinergia = walk.get(WalkerContract.C_SINERGIA) as ArrayList<String>
                    )
                )
            }
            listWalkers.value = walkers
        }
    }


       fun getWalker(profile: String){
           var walker = WalkerModel()
           useCase.obtWalker(profile).addOnSuccessListener { walkerdb ->
               for(walk in walkerdb){
                   walker = WalkerModel(
                       name = walk.get(WalkerContract.C_NAME) as String,
                       price = walk.get(WalkerContract.C_PRICE) as Number,
                       rating = walk.get(WalkerContract.C_RATING) as Number,
                       profile = walk.get(WalkerContract.C_PROFILE) as String,
                       image = walk.get(WalkerContract.C_IMG) as String,
                   )
               }
               walkerData.value = walker
           }
       }


    fun obtBestWalkers(){
        useCase.obtBestWalkers().addOnSuccessListener { walkersdb ->
            val walkers = ArrayList<WalkerModel>()
            for (walk in walkersdb) {
                walkers.add(
                    WalkerModel(name = walk.get(WalkerContract.C_NAME) as String,
                        price = walk.get(WalkerContract.C_PRICE) as Number,
                        rating = walk.get(WalkerContract.C_RATING) as Number,
                        profile = walk.get(WalkerContract.C_PROFILE) as String,
                        image = walk.get(WalkerContract.C_IMG) as String,
                        location = walk.getGeoPoint("location") as GeoPoint,
                    )
                )
            }
            listWalkers.value = walkers
        }
    }
    fun searchWalker(walkerName: String){
        useCase.searchWalker(walkerName).addOnSuccessListener { walkersdb ->
            val walkers = ArrayList<WalkerModel>()
            for(walk in walkersdb){
                walkers.add(
                    WalkerModel(
                        name = walk.get(WalkerContract.C_NAME) as String,
                        price = walk.get(WalkerContract.C_PRICE) as Number,
                        rating = walk.get(WalkerContract.C_RATING) as Number,
                        profile = walk.get(WalkerContract.C_PROFILE) as String,
                        image = walk.get(WalkerContract.C_IMG) as String,
                        location = walk.getGeoPoint("location") as GeoPoint,
                    )
                )
            }
            listWalkers.value = walkers
        }
    }
}