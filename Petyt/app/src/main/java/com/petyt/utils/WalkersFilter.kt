package com.petyt.utils

import android.location.Location
import com.petyt.models.WalkerModel

class WalkersFilter {

    fun byLocation(walkers: ArrayList<WalkerModel>, userLocation: Location): ArrayList<WalkerModel>{
        val newWalkers = ArrayList<WalkerModel>()
        for(wa in walkers){
            val locTem = Location("")
            locTem.longitude = wa.location.longitude
            locTem.latitude = wa.location.latitude
            val distance = userLocation.distanceTo(locTem)
            if(distance < 5150){
                wa.distance = distance
                newWalkers.add(wa)
            }
        }
        return newWalkers
    }

    fun byTags(walkers: ArrayList<WalkerModel>, tags: ArrayList<String>): ArrayList<WalkerModel>{
        val newWalkers = ArrayList<WalkerModel>()
        for(walk in walkers){
            var empaty = 0
            if(walk.sinergia[0] == tags.get(0)){ empaty +=1 }
            if(walk.sinergia[1] == tags.get(1)){ empaty +=1 }
            if(walk.sinergia[2] == tags.get(2)){ empaty +=1 }
            if(walk.sinergia[3] == tags.get(3)){ empaty +=1 }
            if(empaty >= 2) { newWalkers.add(walk)}
        }
        return newWalkers
    }
}