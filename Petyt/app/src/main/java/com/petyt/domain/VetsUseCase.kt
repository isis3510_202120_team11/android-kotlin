package com.petyt.domain

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.QuerySnapshot
import com.petyt.data.VetData

class VetsUseCase {

    private val vetsData = VetData()

    suspend fun getVets(): Task<QuerySnapshot>{
        return vetsData.getVets()
    }

    fun obtVet(mail:String): Task<QuerySnapshot>{
        return vetsData.obtVet(mail)
    }

    fun obtBestVets(): Task<QuerySnapshot>{
        return vetsData.obtBestVets()
    }

    fun searchVet(vetsName: String): Task<QuerySnapshot>{
        return vetsData.searchVet(vetsName)
    }
}