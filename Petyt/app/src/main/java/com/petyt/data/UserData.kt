package com.petyt.data

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.petyt.contracts.UserContract
import com.petyt.models.UserModel

class UserData {
    private val db = FirebaseFirestore.getInstance() //crear una instacia de firestore
    private val contract = UserContract //contrato de usuario
    private var userCollection = db.collection(contract.N_TABLA)

    fun loginUser(email: String, pass: String): Task<QuerySnapshot> {
        return userCollection
            .whereEqualTo(contract.C_MAIL, email)
            .whereEqualTo(contract.C_PASS, pass).get()
    }
    fun addUser(user: UserModel): Task<DocumentReference> {
        return userCollection.add(
            hashMapOf(
                contract.C_NAME to user.name, contract.C_MAIL to user.mail,
                contract.C_PASS to user.password, contract.C_GENDER to user.gender,
                contract.C_LOCATION to user.location, contract.C_BIRTH to user.birth,
                contract.C_IMG to user.image
            )
        )
    }
    fun getUser(user: String): Task<QuerySnapshot>{
        return userCollection.whereEqualTo(contract.C_MAIL, user).get()
    }
}