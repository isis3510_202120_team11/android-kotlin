package com.petyt.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.petyt.contracts.PetContract
import com.petyt.domain.PetsUseCase
import com.petyt.models.PetModel
import kotlinx.coroutines.launch

class PetViewModel() : ViewModel() {
    val petList = MutableLiveData<ArrayList<PetModel>>()
    val result = MutableLiveData<Boolean>()

    fun onCreate(owner: String) {
        viewModelScope.launch {
            getPetList(owner)
            result.value = false
        }
    }
    fun getPetList(owner: String){
        PetsUseCase().getPets(owner).addOnSuccessListener { petsdb ->
            val pets = ArrayList<PetModel>()
            for (pet in petsdb) {
                pets.add(
                    PetModel(
                        name = pet.get(PetContract.C_NAME) as String,
                        sex = pet.get(PetContract.C_SEX) as String,
                        age = pet.get(PetContract.C_AGE) as Number,
                        breed = pet.get(PetContract.C_BREED) as String,
                        imageStr = pet.get(PetContract.C_IMG) as String,
                        humor = pet.get(PetContract.C_HUMOR) as String,
                        size = pet.get(PetContract.C_SIZE) as String
                    )
                )
            }
            Log.d("mascotas", pets.size.toString())
            petList.value = pets
        }
    }
    fun addPet(pet: PetModel){
        PetsUseCase().addPet(pet)
            .addOnSuccessListener { result.value = true }
            .addOnFailureListener { result.value = false }
    }
}