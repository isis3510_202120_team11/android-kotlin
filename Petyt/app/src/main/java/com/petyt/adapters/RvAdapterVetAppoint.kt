package com.petyt.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.petyt.R
import com.petyt.interfaces.ClickListenerPet
import com.petyt.interfaces.ClickListenerVet
import com.petyt.models.VetAppointModel

class RvAdapterVetAppoint(vetsList: ArrayList<VetAppointModel>, var clickListen: ClickListenerPet):RecyclerView.Adapter<RvAdapterVetAppoint.viewHold>() {
    var vets: ArrayList<VetAppointModel> = vetsList
    lateinit var viewHolder: viewHold

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHold {
        val vista = LayoutInflater.from(parent.context).inflate(R.layout.vet_appoints_view, parent, false)
        viewHolder = viewHold(vista, clickListen)
        return viewHolder
    }
    override fun onBindViewHolder(holder: viewHold, position: Int) {
        val appoint = vets[position]
        holder.fecha.text = appoint.fecha
        holder.hora.text = appoint.hora
        holder.price.text = appoint.price.toString()
        holder.walker.text = appoint.provider

    }

    override fun getItemCount(): Int {
        return vets.size
    }

    class viewHold(var vista: View, listen: ClickListenerPet): RecyclerView.ViewHolder(vista), View.OnClickListener{
        var fecha: TextView
        var hora: TextView
        var price: TextView
        var walker: TextView


        var listener: ClickListenerPet

        init {
            this.listener = listen
            fecha = vista.findViewById(R.id.tv_ap_fecha)
            hora = vista.findViewById(R.id.tv_ap_hora)
            price = vista.findViewById(R.id.tv_costo_ap)
            walker = vista.findViewById(R.id.tv_paseador_ap)


            vista.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            this.listener.clickPet(v!!, adapterPosition)
        }
    }
}