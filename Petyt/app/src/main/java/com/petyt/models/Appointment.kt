package com.petyt.models

class Appointment(fecha:String = "", hora:String = "", price: Number = 0, provider: String = "", client: String = "", petName: String ="", walkTime: Number = 0) {
    var fecha:String = ""
    var hora:String = ""
    var price: Number = 0
    var provider: String = ""
    var client: String = ""
    var pet:String =""
    var walkTime: Number = 0

    init {
        this.client = client
        this.fecha = fecha
        this.hora = hora
        this.provider = provider
        this.price = price
        this.pet = petName
        this.walkTime = walkTime
    }
}