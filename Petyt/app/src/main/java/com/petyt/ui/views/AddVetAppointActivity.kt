package com.petyt.ui.views

import com.petyt.utils.SharedPreferences
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import com.petyt.R
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.petyt.models.VetAppointModel
import com.petyt.models.VetModel
import com.petyt.utils.ConectivityCheck
import com.petyt.viewmodel.PetViewModel
import com.petyt.viewmodel.VetAppointViewModel
import com.petyt.viewmodel.VetViewModel

class AddVetAppointActivity: AppCompatActivity() {

    lateinit var btnSaveAgenda:  Button
    lateinit var spHours: Spinner
    lateinit var spMinutes: Spinner
    lateinit var spNoon: Spinner
    lateinit var spDay: Spinner
    lateinit var spMonth: Spinner
    lateinit var spYear: Spinner
    lateinit var spPet: Spinner
    lateinit var etCost: TextView

    val petViewModel: PetViewModel by viewModels()
    val vetViewModel: VetViewModel by viewModels()
    val vetAppointViewModel: VetAppointViewModel by viewModels()

    var vetObj = VetModel()
    var pet =""
    var provider = ""
    var costo = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_vet_appoint)

        petViewModel.onCreate(SharedPreferences(this).getOwner())
        vetAppointViewModel.onCreate(this)
        vetViewModel.onCreate()

        provider = intent.getStringExtra("vets").toString()
        btnSaveAgenda = findViewById(R.id.btn_save_vap)
        spHours = findViewById(R.id.sp_hora_vap)
        spMinutes = findViewById(R.id.sp_minutos_vap)
        spNoon = findViewById(R.id.sp_time_vap)
        spDay = findViewById(R.id.sp_dia_vap)
        spMonth = findViewById(R.id.sp_mes_vap)
        spYear = findViewById(R.id.sp_year_vap)
        spPet = findViewById(R.id.sp_mascotas_vap)
        etCost = findViewById(R.id.tv_coste_vap)
        spYear.lastVisiblePosition
        btnSaveAgenda.setOnClickListener{ saveVetAppoint() }
        btnSaveAgenda.isEnabled = false

        petViewModel.petList.observe(this, {obtMascotas()})

        vetViewModel.vetData.observe(this, {
            vetObj = it
            val cost = "$ ${vetObj.rate}"
            etCost.text = cost

        })

        vetAppointViewModel.results.observe(this, {
            if(it){
                Toast.makeText(this, "Fecha Agendada", android.widget.Toast.LENGTH_SHORT).show()
                finish()
            }
            else{
               Toast.makeText(this, "Error al agendar", android.widget.Toast.LENGTH_SHORT).show()
            }
        })

        vetViewModel.getVet(provider)
    }

    fun saveVetAppoint(){
        
        if(ConectivityCheck.isConnected(this)){
            val fecha = "${spDay.selectedItem}/${spMonth.selectedItem}/${spYear.selectedItem}"
            val hora = "${spHours.selectedItem}:${spMinutes.selectedItem} ${spNoon.selectedItem}"
            val client = SharedPreferences(this).getOwner()

            if(fecha.isEmpty()) return Toast.makeText(this, "Debes ingresar una fecha", Toast.LENGTH_SHORT).show()

            val vap = VetAppointModel(
                fecha= fecha,
                hora = hora,
                provider = vetObj.name,
                client = client,
                petName = pet,
                price = vetObj.rate
            )
            vetAppointViewModel.addVetAppoint(vap)
        }
        else{
            Toast.makeText(this, "Debes tener conexión a internet para agendar una cita", Toast.LENGTH_SHORT).show()
        }


    }

    private fun obtMascotas(){
        val mascotas = ArrayList<String>()
        for(mas in petViewModel.petList.value!!){
            mascotas.add(mas.name)
        }
        if(mascotas.size == 0){
            Toast.makeText(this, "Debes agregar una mascota primero", Toast.LENGTH_SHORT).show()
            btnSaveAgenda.isEnabled = false
            return
        }
        setupMascotas(mascotas)
    }

    private fun setupMascotas(mascotas: ArrayList<String>){
        spPet.adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, mascotas)
        spPet.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                pet = mascotas[position]
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(this@AddVetAppointActivity, "Debes elegir una mascota primero", Toast.LENGTH_SHORT).show()
            }
        }
        pet = mascotas[0]
        btnSaveAgenda.isEnabled = true
    }

}