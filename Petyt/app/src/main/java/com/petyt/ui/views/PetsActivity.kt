package com.petyt.ui.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.petyt.R
import com.petyt.adapters.RvAdapterPets
import com.petyt.utils.SharedPreferences
import com.petyt.interfaces.ClickListenerPet
import com.petyt.viewmodel.PetViewModel

class PetsActivity : AppCompatActivity() {

    lateinit var rvPets: RecyclerView
    lateinit var fabAddPet: FloatingActionButton
    lateinit var btnhome: Button
    lateinit var btnAgenda: Button

    lateinit var adapterLista: RvAdapterPets
    lateinit var layoutMan: RecyclerView.LayoutManager
    var owner = ""
    val petViewModel: PetViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pets)
        owner = SharedPreferences(this).getOwner()
        petViewModel.onCreate(owner)

        rvPets = findViewById(R.id.rv_pets)
        fabAddPet = findViewById(R.id.fab_add_pets)
        btnhome = findViewById(R.id.btn_home_pet)
        btnAgenda = findViewById(R.id.btn_agenda_pet)
        rvPets.setHasFixedSize(true)
        layoutMan = LinearLayoutManager(this)
        fabAddPet.setOnClickListener { goAddPet() }
        btnhome.setOnClickListener { finish() }
        btnAgenda.setOnClickListener { finish() }
        petViewModel.petList.observe(this, { setupRecycler() })
    }
    override fun onResume() {
        petViewModel.getPetList(owner)
        super.onResume()
    }
    fun setupRecycler(){
        adapterLista = RvAdapterPets(petViewModel.petList.value!!, object : ClickListenerPet {
            override fun clickPet(vista: View, index: Int) {
                Toast.makeText(this@PetsActivity, "mascota tocada", Toast.LENGTH_SHORT).show()
            }
        })
        rvPets.layoutManager = layoutMan
        rvPets.adapter = adapterLista
    }

    fun goAddPet(){
        startActivity(Intent(this, AddPetActivity::class.java))
    }
}