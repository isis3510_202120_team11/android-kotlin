package com.petyt.domain

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.QuerySnapshot
import com.petyt.data.PetsData
import com.petyt.models.PetModel

class PetsUseCase {

    fun getPets(owner: String): Task<QuerySnapshot>{
        return PetsData().getPets(owner)
    }

    fun addPet(pet: PetModel): Task<DocumentReference>{
        return PetsData().addPet(pet)
    }
}