package com.petyt.utils

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Looper
import android.provider.Settings
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*

class LocationUtil(var activity: Activity) {

    var latitud: String = ""
    var longitud: String = ""
    lateinit var fused: FusedLocationProviderClient
    lateinit var mLocationCallBack: LocationCallback
    var mLocationRequest = LocationRequest()
    val REQUEST_LOCATION = 111
    val REQUIRED_PERMISSIONS_GPS =
        arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

    fun checkPermissionGranted(){
        if (allPermissionsGrantedGPS()){
            fused = LocationServices.getFusedLocationProviderClient(activity)
            setCallback()
        } else {
            ActivityCompat.requestPermissions(activity,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_LOCATION
            )
        }
    }
    fun getUbications(): Boolean{
        if (checkPermissions()){
             if (isLocationEnabled()){
                 if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                     requestNewLocationData()
                     return true
                }
            } else {
                 Toast.makeText(activity, "Debes activar la ubicación", Toast.LENGTH_SHORT).show()
                 val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                 activity.startActivity(intent)
                 activity.finish()
                return false
            }
            return false
        } else {
            ActivityCompat.requestPermissions(activity,
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_LOCATION)
            Toast.makeText(activity, "Debes otorgar permisos de GPS", Toast.LENGTH_SHORT).show()
            return false
        }
    }
    @SuppressLint("MissingPermission")
    fun requestNewLocationData(){
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1
        fused = LocationServices.getFusedLocationProviderClient(activity)
        fused.requestLocationUpdates(mLocationRequest, mLocationCallBack, Looper.myLooper())
    }
    private fun  setCallback() {
        mLocationCallBack = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                val mLastLocation: Location = locationResult.lastLocation
                latitud = mLastLocation.latitude.toString()
                longitud = mLastLocation.longitude.toString()
            }
        }
    }
    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }
    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun allPermissionsGrantedGPS() = REQUIRED_PERMISSIONS_GPS.all { permission ->
        ContextCompat.checkSelfPermission(activity.baseContext, permission) == PackageManager.PERMISSION_GRANTED
    }
}