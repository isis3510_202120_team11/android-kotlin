package com.petyt.ui.views

import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.widget.Toolbar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.GeoPoint
import com.petyt.R
import com.petyt.utils.ConectivityCheck
import com.petyt.utils.LocationUtil
import com.petyt.utils.SharedPreferences
import com.petyt.viewmodel.UserViewModel

class ServicesActivity : AppCompatActivity() {

    lateinit var petService: ImageButton
    lateinit var walkerService: ImageButton
    lateinit var vetsService: ImageButton
    lateinit var btnRecomend: Button
    lateinit var fabLocation: FloatingActionButton
    lateinit var tolbar: Toolbar
    private lateinit var LocationUtil: LocationUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_services)
        petService = findViewById(R.id.ibt_pets)
        walkerService = findViewById(R.id.ibt_walkers)
        vetsService = findViewById(R.id.ibt_vets)

        btnRecomend = findViewById(R.id.btn_recomended)
        fabLocation = findViewById(R.id.fab_location)

        petService.setOnClickListener { startActivity(Intent(this, PetsActivity::class.java)) }
        walkerService.setOnClickListener { startActivity(Intent(this, WalkersActivity::class.java)) }
        //vetsService.setOnClickListener{ startActivity(Intent(this, VetsActivity::class.java))}
        btnRecomend.setOnClickListener { startActivity(Intent(this, BestWalkersActivity::class.java)) }
        fabLocation.setOnClickListener { getUbication() }
        vetsService.setOnClickListener { goVets() }
        LocationUtil = LocationUtil(this)
        setToolbar()
        getUbication()
    }
    private fun setToolbar(){
        tolbar = findViewById(R.id.toolbar)
        tolbar.setTitle(R.string.app_name)
        setSupportActionBar(tolbar)
        tolbar.setNavigationOnClickListener{ finish() }
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_logout, menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.it_logout -> { logout() }
        }
        return super.onOptionsItemSelected(item)
    }
    private fun logout(){
        FirebaseAuth.getInstance().signOut()
        SharedPreferences(this).clearCommit()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun goVets(){
        if(ConectivityCheck.isConnected(this)){
            startActivity(Intent(this, VetsActivity::class.java))
        }
        else{
            Toast.makeText(this, "No tienes conexión intente más tarde", Toast.LENGTH_SHORT).show()
        }
    }


    @SuppressLint("MissingPermission")
    private fun getUbication(){
        LocationUtil.checkPermissionGranted()
        if(LocationUtil.getUbications()){
            LocationUtil.fused.lastLocation.
            addOnCompleteListener { task ->
                val locate: Location? = task.result
                if (locate == null){
                    Toast.makeText(this, "no se pudo obtener la ubicación actual", Toast.LENGTH_SHORT).show()
                    LocationUtil.requestNewLocationData()
                } else {
                    SharedPreferences(this).saveLocation(locate.latitude, locate.longitude)
                }
            }
        }
    }
}