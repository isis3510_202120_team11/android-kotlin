package com.petyt.domain

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.QuerySnapshot
import com.petyt.data.UserData
import com.petyt.models.UserModel

class UserUseCase {
    fun loginUseCase(mail: String, pass: String): Task<QuerySnapshot>{
        return UserData().loginUser(mail, pass)
    }
    fun registerUser(user: UserModel): Task<DocumentReference>{
        return UserData().addUser(user)
    }
    fun getUser(user: String): Task<QuerySnapshot>{
        return UserData().getUser(user)
    }
}