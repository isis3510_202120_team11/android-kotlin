package com.petyt.data

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.petyt.contracts.AppointContract
import com.petyt.models.AppointModel

class AppointData {
    private val db = FirebaseFirestore.getInstance()
    private val contract = AppointContract
    private var apCollection = db.collection(contract.N_TABLA)

    fun addAppoint(ap: AppointModel): Task<DocumentReference> {
        return apCollection.add(
            hashMapOf(
                contract.C_TIME to ap.walkTime,
                contract.C_PRICE to ap.price,
                contract.C_PET to ap.petName,
                contract.C_CLIENT to ap.client,
                contract.C_PROVIDER to ap.provider,
                contract.C_HORA to ap.hora,
                contract.C_FECHA to ap.fecha
            )
        )
    }
    fun getAppoints(client: String): Task<QuerySnapshot> {
        return apCollection
            .whereEqualTo(contract.C_CLIENT, client).get()
    }
}
