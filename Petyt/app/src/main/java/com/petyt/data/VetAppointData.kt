package com.petyt.data

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.petyt.contracts.VetAppointContract
import com.petyt.models.VetAppointModel

class VetAppointData {
    private val db = FirebaseFirestore.getInstance()
    private val contract = VetAppointContract
    private var vapCollection = db.collection(contract.N_TABLA)

    fun addAppoint(vap: VetAppointModel): Task<DocumentReference>{
        return vapCollection.add(
            hashMapOf(
                contract.C_PRICE to vap.price,
                contract.C_PET to vap.petName,
                contract.C_CLIENT to vap.client,
                contract.C_PROVIDER to vap.provider,
                contract.C_HORA to vap.hora,
                contract.C_FECHA to vap.fecha
            )
        )
    }

    fun getAppoints(client: String): Task<QuerySnapshot> {
        return vapCollection
            .whereEqualTo(contract.C_CLIENT, client).get()
    }
}