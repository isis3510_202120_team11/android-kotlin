package com.petyt.ui.views

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.location.Location
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.widget.*
import androidx.activity.viewModels
import com.google.firebase.auth.FirebaseAuth
import com.petyt.R
import com.petyt.utils.*
import androidx.lifecycle.Observer
import com.google.firebase.firestore.GeoPoint
import com.petyt.databinding.ActivityRegisterBinding
import com.petyt.models.UserModel
import com.petyt.viewmodel.UserViewModel
import java.util.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding : ActivityRegisterBinding
    lateinit var etNombre: EditText
    lateinit var etCorreo: EditText
    lateinit var etContra1: EditText
    lateinit var etContra2: EditText
    lateinit var etGender: EditText
    lateinit var btnRegistrar: Button
    lateinit var btnAddPhoto: Button
    lateinit var btnAddPhotoAlbum: Button
    lateinit var btnUbucacion: Button
    lateinit var ivPhoto: ImageView
    lateinit var spDay: Spinner
    lateinit var spYear: Spinner
    lateinit var spMonth: Spinner
    var location: GeoPoint = GeoPoint(0.0,0.0)

    val REQUEST_IMAGE_CAPTURE = 1
    val REQUEST_GALLERY = 110
    lateinit var imageUri: Uri
    lateinit var image: Bitmap
    private var imageStr: String = ""
    lateinit var shared: SharedPreferences
    private val auth = FirebaseAuth.getInstance()
    val userViewModel: UserViewModel by viewModels()
    private lateinit var LocationUtil: LocationUtil


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //setContentView(R.layout.activity_register)

        userViewModel.onCreate()
        LocationUtil = LocationUtil(this)

        etNombre = binding.etNombreRegistro
        etCorreo = binding.etCorreoRegistro
        etContra1 = binding.etContra1Registro
        etContra2 = binding.etContra2Registro
        btnRegistrar = binding.btnRegistrar
        btnAddPhotoAlbum = binding.btnFotoRegistro
        btnAddPhoto = binding.btnAlbumRegistro
        btnUbucacion = binding.btnUbicacionRegistro
        spDay = binding.spDiaRegistro
        spMonth = binding.spMesRegistro
        spYear = binding.spYearRegistro
        ivPhoto = binding.ivFotoRegistro
        etGender = binding.etGenderRegistro

        //etNombre = findViewById(R.id.et_nombre_registro)
        //etCorreo = findViewById(R.id.et_correo_registro)
        //etContra1 = findViewById(R.id.et_contra1_registro)
        //etContra2 = findViewById(R.id.et_contra2_registro)
        //btnRegistrar = findViewById(R.id.btn_registrar)
        //btnAddPhotoAlbum = findViewById(R.id.btn_foto_registro)
        //btnAddPhoto = findViewById(R.id.btn_album_registro)
        //btnUbucacion = findViewById(R.id.btn_ubicacion_registro)
        //spDay = findViewById(R.id.sp_dia_registro)
        //spMonth = findViewById(R.id.sp_mes_registro)
        //spYear = findViewById(R.id.sp_year_registro)
        //ivPhoto = findViewById(R.id.iv_foto_registro)
        //etGender = findViewById(R.id.et_gender_registro)

        shared = SharedPreferences(this)
        btnRegistrar.setOnClickListener { crearRegistro() }
        btnAddPhoto.setOnClickListener { selectPhoto() }
        btnAddPhotoAlbum.setOnClickListener { takePhoto() }
        btnUbucacion.setOnClickListener { getUbication() }
        defineObserver()
    }
    private fun defineObserver(){
        userViewModel.results.observe(this, {
            if(it){
                Toast.makeText(this, "Registro completo", Toast.LENGTH_SHORT).show()
                val intent = Intent(this, LoginActivity::class.java)
                shared.saveUserMail(etCorreo.text.toString())
                startActivity(intent)
                finish()
            }
            else{ Toast.makeText(this, "Registro falló", Toast.LENGTH_SHORT).show() }
        })
    }
    private fun crearRegistro(){
        val name = etNombre.text //obtengo el valor que haya escrito en el edit text
        val mail = etCorreo.text
        val pass = etContra1.text
        val gender = etGender.text
        val birth = "${spDay.selectedItem}/${spMonth.selectedItem}/${spYear.selectedItem}"
        if(name.isEmpty()) {
            etNombre.requestFocus()
            return Toast.makeText(this, "Debes ingresar tu nombre", Toast.LENGTH_SHORT).show()
        }
        if(mail.isEmpty()) return Toast.makeText(this, "Debes ingresar un correo válido", Toast.LENGTH_SHORT).show()
        if(location.latitude == 0.0) return Toast.makeText(this, "Debes permitir tu ubicación", Toast.LENGTH_SHORT).show()
        if(gender.isEmpty()) return Toast.makeText(this, "Debes ingresar tu género", Toast.LENGTH_SHORT).show()
        if(imageStr.isEmpty()) return Toast.makeText(this, "Debes elegir una imagen de perfil o tomarte una foto", Toast.LENGTH_SHORT).show()
        if (!evalPassword()) return Toast.makeText(this, "las contraseñas deben coincidir", Toast.LENGTH_SHORT).show()
        val user = UserModel(name.toString(), mail.toString(), cifrado.encriptar(pass.toString()), gender.toString(), birth, imageStr, location)
        auth.createUserWithEmailAndPassword(user.mail, user.password).addOnCompleteListener { newUser ->
            if (!newUser.isSuccessful){
                Log.d("error", newUser.exception!!.message!!.toString())
                Toast.makeText(this, "Error al realizar el registro", Toast.LENGTH_SHORT).show()
                return@addOnCompleteListener
            }
            userViewModel.register(user) //método del viewModel para registrar
        }
    }
    private fun evalPassword(): Boolean{
        return etContra1.text.toString() == etContra2.text.toString() && etContra1.text.toString() != ""
    }
    @SuppressLint("QueryPermissionsNeeded")// nos permite omitir posibles errore4s, en este caso, la omision de permisos
    private fun takePhoto() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }
    private fun selectPhoto(){
        galeriaUtil().seleccionarFoto(this, REQUEST_GALLERY)
    }
    @SuppressLint("MissingPermission")
    private fun getUbication(){
        LocationUtil.checkPermissionGranted()
        if(LocationUtil.getUbications()){
            LocationUtil.fused.lastLocation.
            addOnCompleteListener { task ->
                val locate: Location? = task.result
                if (locate == null){
                    LocationUtil.requestNewLocationData()
                    getUbication()
                } else {
                    Toast.makeText(this, locate.latitude.toString(), Toast.LENGTH_SHORT).show()
                    location = GeoPoint(locate.latitude, locate.longitude)
                }
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            data?.extras?.let { bundle ->
                image = bundle.get("data") as Bitmap
                ivPhoto.setImageBitmap(image)
                imageStr = Base64Converter().getBase64Image(image)
            }
        }
        if(requestCode == REQUEST_GALLERY && resultCode == RESULT_OK){
            imageUri = data!!.data!!
            ivPhoto.setImageURI(imageUri)
            val bitmap = (ivPhoto.drawable as BitmapDrawable).bitmap
            imageStr = Base64Converter().getBase64Image(bitmap)
        }
    }
}