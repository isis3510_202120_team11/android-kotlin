package com.petyt.viewmodel

import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.GeoPoint
import com.petyt.contracts.VetContract
import com.petyt.contracts.WalkerContract
import com.petyt.domain.VetsUseCase
import com.petyt.models.VetModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.Dispatcher

class VetViewModel: ViewModel() {
    val listVets = MutableLiveData<ArrayList<VetModel>>()
    val vetData = MutableLiveData<VetModel>()
    val locate = MutableLiveData<Location>()
    private val useCase = VetsUseCase()

    fun onCreate(){
        viewModelScope.launch {
            locate.value  = Location("")
        }
    }

    fun getVetList(){

        viewModelScope.launch(Dispatchers.IO) {
            useCase.getVets().addOnSuccessListener { vetsdb ->
                val vets = ArrayList<VetModel>()
                for (vet in vetsdb) {
                    vets.add(
                        VetModel(
                            name = vet.get(VetContract.C_NAME) as String,
                            rate = vet.get(VetContract.C_RATE) as Number,
                            rating = vet.get(VetContract.C_RATING) as Number,
                            profile = vet.get(VetContract.C_PROFILE) as String,
                            image = vet.get(VetContract.C_IMG) as String,
                            location = vet.getGeoPoint("location") as GeoPoint,
                            sinergia = vet.get(VetContract.C_SINERGIA) as ArrayList<String>,
                            specialty = vet.get(VetContract.C_SPECIALTY) as String
                        )
                    )
                }
                listVets.value = vets
            }
        }
    }

    fun getVet(profile:String){
        var vete = VetModel()
        useCase.obtVet(profile).addOnSuccessListener { vetdb ->
            for(vet in vetdb){
                vete = VetModel(
                    name = vet.get(VetContract.C_NAME) as String,
                    rate = vet.get(VetContract.C_RATE) as Number,
                    rating = vet.get(VetContract.C_RATING) as Number,
                    profile = vet.get(VetContract.C_PROFILE) as String,
                    image = vet.get(VetContract.C_IMG) as String,
                    specialty = vet.get(VetContract.C_SPECIALTY) as String
                )
            }
            vetData.value = vete
        }
    }

    fun obtBestVets(){
        useCase.obtBestVets().addOnSuccessListener { vetsdb ->
            val vets = ArrayList<VetModel>()
            for(vet in vetsdb){
                vets.add(
                    VetModel(name = vet.get(VetContract.C_NAME) as String,
                        rate = vet.get(VetContract.C_RATE) as Number,
                        rating = vet.get(VetContract.C_RATING) as Number,
                        profile = vet.get(VetContract.C_PROFILE) as String,
                        image = vet.get(VetContract.C_IMG) as String,
                        specialty = vet.get(VetContract.C_SPECIALTY) as String,
                        location = vet.getGeoPoint("location") as GeoPoint,
                    )
                )
            }
            listVets.value = vets
        }

    }

    fun searchVet(vetName: String){
        useCase.searchVet(vetName).addOnSuccessListener { vetsdb ->
            val vets = ArrayList<VetModel>()
            for(vet in vetsdb){
                vets.add(
                    VetModel(
                        name = vet.get(VetContract.C_NAME) as String,
                        rate = vet.get(VetContract.C_RATE) as Number,
                        rating = vet.get(VetContract.C_RATING) as Number,
                        profile = vet.get(VetContract.C_PROFILE) as String,
                        image = vet.get(VetContract.C_IMG) as String,
                        specialty = vet.get(VetContract.C_SPECIALTY) as String,
                        location = vet.getGeoPoint("location") as GeoPoint,
                        )
                )
            }
            listVets.value = vets
        }

    }
}