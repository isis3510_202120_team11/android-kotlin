package com.petyt.utils

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.petyt.contracts.AppointContract
import com.petyt.db.DbHelper
import com.petyt.models.AppointModel

class AppointLocal(context: Context) {
    private var dbHelper: DbHelper? = null
    private val n_tabla = AppointContract.N_TABLA
    private val c_id = AppointContract.C_ID
    private val c_fecha = AppointContract.C_FECHA
    private val c_hora = AppointContract.C_HORA
    private val c_prov = AppointContract.C_TIME
    private val c_client = AppointContract.C_CLIENT
    private val c_pet = AppointContract.C_PET
    private val c_price = AppointContract.C_PRICE
    private val c_time = AppointContract.C_TIME

    init {
        dbHelper = DbHelper(context)
    }

    suspend fun addAppoint(appoint: AppointModel){
        val db: SQLiteDatabase = dbHelper?.writableDatabase!!
        val values = ContentValues().apply {
            put(c_fecha, appoint.fecha)
            put(c_hora, appoint.hora)
            put(c_prov, appoint.provider)
            put(c_client, appoint.client)
            put(c_pet, appoint.petName)
            put(c_price, appoint.price.toFloat())
            put(c_time, appoint.walkTime.toInt())

        }
        val newRow = db.insert(n_tabla, null, values)
        db.close()
    }

     fun getAppo(): ArrayList<AppointModel> {
        val items: ArrayList<AppointModel> = ArrayList()
        val db: SQLiteDatabase = dbHelper?.readableDatabase!!
        val columnas = arrayOf(c_fecha, c_hora, c_prov, c_pet, c_price, c_time)
        val c: Cursor = db.query(n_tabla, columnas, null, null, null, null, null)
        while (c.moveToNext()){
            items.add(AppointModel(
                fecha = c.getString(c.getColumnIndexOrThrow(c_fecha)),
                hora = c.getString(c.getColumnIndexOrThrow(c_hora)),
                petName = c.getString(c.getColumnIndexOrThrow(c_pet)),
                price = c.getFloat(c.getColumnIndexOrThrow(c_price)) as Number,
                walkTime = c.getInt(c.getColumnIndexOrThrow(c_time)) as Number
            ))
        }
        db.close()
        return items
    }
}