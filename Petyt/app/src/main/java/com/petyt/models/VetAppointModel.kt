package com.petyt.models

data class VetAppointModel (
    val fecha:String = "",
    val hora:String = "",
    val price: Number = 0,
    val provider: String = "",
    val client: String = "",
    val petName: String =""
)