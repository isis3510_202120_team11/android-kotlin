package com.petyt.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.petyt.R
import com.petyt.interfaces.ClickListenerWalker
import com.petyt.models.WalkerModel
import com.squareup.picasso.Picasso
import java.text.DecimalFormat

class RvAdapterWalkers(var walkersList:ArrayList<WalkerModel>,
                       var clickListen: ClickListenerWalker): RecyclerView.Adapter<RvAdapterWalkers.viewHold>() {

    var walkers = walkersList
    lateinit var viewHolder: viewHold

    //cargamos la vista
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHold {
        val vista = LayoutInflater.from(parent.context).inflate(R.layout.walker_view, parent, false) //integracion entre vista y adaptador
        viewHolder = viewHold(vista, clickListen)
        return viewHolder
    }
    //llenamos la informacion de cada walker en la vista (walker_view.xml)
    override fun onBindViewHolder(holder: viewHold, position: Int) {
        val walker = walkers[position]
        holder.name.text = walker.name
        val precio = "$${walker.price} x hora"
        holder.price.text = precio
        holder.rating.rating = walker.rating.toFloat()
        Picasso.get().load(walker.image).into(holder.image);
        if (walker.distance != 0.0F){
            val format = DecimalFormat("#.0")
            val distanceText = "a ${format.format(walker.distance/1000)} km de tí"
            holder.distance.text = distanceText
        }
    }

    override fun getItemCount(): Int {
        return walkers.size
    }

    class viewHold(var vista: View, listen: ClickListenerWalker): RecyclerView.ViewHolder(vista), View.OnClickListener{
        var name: TextView //crear una variable para cada elemento de la vista
        var price: TextView
        var rating: RatingBar
        var image: ImageView
        var distance: TextView
        var listener: ClickListenerWalker

        init {
            this.listener = listen
            name = vista.findViewById(R.id.tv_name_walker) //asociamos cada variable con la vista
            price = vista.findViewById(R.id.tv_price_walker)
            rating = vista.findViewById(R.id.rb_rating_walker)
            image = vista.findViewById(R.id.iv_photo_walker)
            distance = vista.findViewById(R.id.tv_distance_walker)
            vista.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            this.listener.clickWalk(v!!, adapterPosition)
        }
    }
}