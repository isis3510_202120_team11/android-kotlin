package com.petyt.data

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.petyt.contracts.PetContract
import com.petyt.models.PetModel

class PetsData {

    private val db = FirebaseFirestore.getInstance() //crear una instacia de firestore
    private val contract = PetContract //contrato de usuario
    private var petsCollection = db.collection(contract.N_TABLA)

    fun addPet(pet: PetModel): Task<DocumentReference>{
        return petsCollection.add(
            hashMapOf(
                contract.C_NAME to pet.name,
                contract.C_AGE to pet.age,
                contract.C_BIRTH to pet.birth,
                contract.C_BREED to pet.breed,
                contract.C_OWNER to pet.owner,
                contract.C_SEX to pet.sex,
                contract.C_SIZE to pet.size,
                contract.C_HUMOR to pet.humor,
                contract.C_IMG to pet.imageStr
            )
        )
    }

    fun getPets(owner: String): Task<QuerySnapshot> {
        return petsCollection
            .whereEqualTo(contract.C_OWNER, owner).get()
    }
}