package com.petyt.domain

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.QuerySnapshot
import com.petyt.data.WalkerData

class WalkerUseCase {

    private val walkerData = WalkerData()

    fun getWalkers(): Task<QuerySnapshot> {
        return walkerData.getWalkers()
    }

   fun obtWalker(mail:String): Task<QuerySnapshot> {
        return walkerData.obtWalker(mail)
    }


    fun obtBestWalkers(): Task<QuerySnapshot> {
        return walkerData.obtBestWalkers()
    }

    fun searchWalker(walkerName: String): Task<QuerySnapshot>{
        return walkerData.searchWalkers(walkerName)
    }
}