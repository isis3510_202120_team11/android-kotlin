package com.petyt.contracts

class VetContract {
    companion object{
        val N_TABLA = "vets"
        val C_NAME = "name"
        val C_PROFILE = "profile"
        val C_RATE = "rate"
        val C_RATING = "rating"
        val C_IMG = "img"
        val C_SINERGIA = "sinergia"
        val C_SPECIALTY = "specialty"
        val C_ID = "id"
    }
}