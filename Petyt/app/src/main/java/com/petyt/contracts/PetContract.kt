package com.petyt.contracts

class PetContract {
    companion object{
        val N_TABLA = "Pets"
        val C_NAME = "name"
        val C_BREED = "breed"
        val C_AGE = "age"
        val C_SEX = "sex"
        val C_OWNER = "owner"
        val C_BIRTH = "birth"
        val C_IMG = "image"
        val C_SIZE = "size"
        val C_HUMOR = "humor"
        val C_ID = "id"
    }
}