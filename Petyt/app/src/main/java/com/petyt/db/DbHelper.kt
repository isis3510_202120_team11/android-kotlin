package com.petyt.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.petyt.contracts.AppointContract

class DbHelper (context: Context): SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION){

    val contract = AppointContract

    private val CREATE_TB_APPOINTMENT = "CREATE TABLE ${contract.N_TABLA} " +
            "(${contract.C_ID} INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "${contract.C_FECHA} TEXT, ${contract.C_HORA} TEXT," +
            "${contract.C_CLIENT} TEXT, ${contract.C_PROVIDER} TEXT,"+
            "${contract.C_PET} TEXT, ${contract.C_PRICE} TEXT, ${contract.C_TIME} INTEGER)"
    private val REMOVE_TB_APPOINTMENT = "DROP TABLE IF EXISTS ${contract.N_TABLA}"

    override fun onCreate(db: SQLiteDatabase?) {
        with(db!!) {
            execSQL(CREATE_TB_APPOINTMENT)

        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        with(db!!){
            execSQL(REMOVE_TB_APPOINTMENT)
        }
        onCreate(db)
    }

    companion object{
        val DB_NAME = "contactos.db"
        val DB_VERSION = 1
    }
}