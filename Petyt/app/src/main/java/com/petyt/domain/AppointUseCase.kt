package com.petyt.domain

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.QuerySnapshot
import com.petyt.data.AppointData
import com.petyt.models.AppointModel

class AppointUseCase {
    fun obtAppoints(user: String): Task<QuerySnapshot>{
        return AppointData().getAppoints(user)
    }
    fun addApoint(ap: AppointModel): Task<DocumentReference>{
        return AppointData().addAppoint(ap)
    }
}