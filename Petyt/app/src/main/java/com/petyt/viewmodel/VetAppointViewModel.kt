package com.petyt.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.petyt.contracts.AppointContract
import com.petyt.domain.VetAppointUseCase
import com.petyt.models.VetAppointModel
import com.petyt.utils.VetAppointLocal
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class VetAppointViewModel: ViewModel() {
    val listVetAppoint = MutableLiveData<ArrayList<VetAppointModel>>()
    val results = MutableLiveData<Boolean>()
    lateinit var VetAppointLocal: VetAppointLocal

    fun onCreate(context: Context){
        viewModelScope.launch {
            VetAppointLocal= VetAppointLocal(context)
        }
    }

    fun getVetAppoints(user:String){
        VetAppointUseCase().obtAppoints(user).addOnSuccessListener { vetsdb ->
            val vetAppoints = ArrayList<VetAppointModel>()
            for(vep in vetsdb){
                vetAppoints.add(
                    VetAppointModel(
                        fecha = vep.get(AppointContract.C_FECHA) as String,
                        hora = vep.get(AppointContract.C_HORA) as String,
                        price = vep.get(AppointContract.C_PRICE) as Number,
                        provider = vep.get(AppointContract.C_PROVIDER) as String
                    )
                )
            }
            listVetAppoint.value = vetAppoints
        }
    }

    fun addVetAppoint(vap: VetAppointModel){
        val time = runBlocking { launch { VetAppointLocal.addVetAppoint(vap) } }
        viewModelScope.launch(Dispatchers.IO) {
            VetAppointUseCase().addAppoint(vap)
                .addOnSuccessListener { results.value = true }
                .addOnFailureListener { results.value = false }
        }
    }
}