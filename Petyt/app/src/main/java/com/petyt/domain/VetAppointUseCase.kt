package com.petyt.domain

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.QuerySnapshot
import com.petyt.data.VetAppointData
import com.petyt.models.VetAppointModel
import kotlinx.coroutines.Dispatchers

class VetAppointUseCase {
    fun obtAppoints(user: String): Task<QuerySnapshot>{
        return VetAppointData().getAppoints(user)
    }
    suspend fun addAppoint(vap: VetAppointModel):Task<DocumentReference> {
        return VetAppointData().addAppoint(vap)
    }
}