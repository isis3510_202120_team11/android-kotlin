package com.petyt.contracts

class WalkerContract {
    companion object {
        val N_TABLA = "walkers"
        val C_NAME = "name"
        val C_PROFILE = "profile"
        val C_PRICE = "rate"
        val C_RATING = "rating"
        val C_IMG = "img"
        val C_SINERGIA = "sinergia"
        val C_ID = "id"
    }
}