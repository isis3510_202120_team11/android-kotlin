package com.petyt.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.petyt.contracts.AppointContract
import com.petyt.domain.AppointUseCase
import com.petyt.models.AppointModel
import com.petyt.utils.AppointLocal
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class AppointViewModel: ViewModel() {
    val listAppoint = MutableLiveData<ArrayList<AppointModel>>()
    val results = MutableLiveData<Boolean>()
    lateinit var AppointLocal: AppointLocal

    fun onCreate(context: Context) {
        viewModelScope.launch {
            AppointLocal = AppointLocal(context)
        }
    }
    fun getAppoints(user: String){
        AppointUseCase().obtAppoints(user).addOnSuccessListener { walkersdb ->
            val appoints = ArrayList<AppointModel>()
            for (walk in walkersdb) {
                appoints.add(
                    AppointModel(
                        fecha = walk.get(AppointContract.C_FECHA) as String,
                        hora = walk.get(AppointContract.C_HORA) as String,
                        price = walk.get(AppointContract.C_PRICE) as Number,
                        walkTime = walk.get(AppointContract.C_TIME) as Number,
                        provider = walk.get(AppointContract.C_PROVIDER) as String
                    )
                )
            }
            listAppoint.value = appoints
        }
    }
    fun addApoint(ap: AppointModel){
        val time = runBlocking { launch { AppointLocal.addAppoint(ap) }  }
        AppointUseCase().addApoint(ap)
            .addOnSuccessListener { results.value = true }
            .addOnFailureListener { results.value = false }
    }
}