package com.petyt.ui.views

import android.annotation.SuppressLint
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.petyt.R
import com.petyt.utils.LocationUtil

class LocationActivity : AppCompatActivity() {

    lateinit var btnUbicacion: Button
    lateinit var tvLatitude: TextView
    lateinit var tvLongitude: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)
        btnUbicacion = findViewById(R.id.btn_ubicacion)
        tvLatitude = findViewById(R.id.tv_latitude)
        tvLongitude = findViewById(R.id.tv_longitude)


    }
}
/*
* R = 6378

Δlat = lat2− lat1

Δlong = long2− long1

a = sin²(Δlat/2) + cos(lat1) · cos(lat2) · sin²(Δlong/2)

c = 2 · atan2(√a, √(1−a))

d = R · c
*
* */

