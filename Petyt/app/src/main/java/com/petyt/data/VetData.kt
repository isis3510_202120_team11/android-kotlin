package com.petyt.data

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.petyt.contracts.VetContract

class VetData {
    private val db = FirebaseFirestore.getInstance()
    private val contract = VetContract
    private var vetsCollection = db.collection(contract.N_TABLA)

    fun getVets(): Task<QuerySnapshot>{
        return vetsCollection.get()
    }

    fun obtVet(mail: String): Task<QuerySnapshot>{
        return vetsCollection.whereEqualTo(contract.C_PROFILE,mail).get()
    }

    fun obtBestVets(): Task<QuerySnapshot>{
        return vetsCollection.whereGreaterThan(contract.C_RATING, 3.7).get()
    }
    //If we have time we can add more like short near, cheaper, rating etp

    fun searchVet(vetsName: String): Task<QuerySnapshot>{
        return vetsCollection.whereEqualTo(contract.C_NAME, vetsName).get()
    }
}