package com.petyt.utils

import android.location.Location
import com.petyt.models.VetModel

class VetsFilter {

    fun vetsByLocation(vets: ArrayList<VetModel>, userLocation: Location): ArrayList<VetModel>{
        val newVets = ArrayList<VetModel>()
        for(vet in vets){
            val vetLocation = Location("")
            vetLocation.longitude = vet.location.longitude
            vetLocation.latitude = vet.location.latitude
            val distance = userLocation.distanceTo(vetLocation)
            if(distance < 5150){
                vet.distance = distance
                newVets.add(vet)
            }

        }
        return newVets
    }

    fun byTags(vets: ArrayList<VetModel>, tags: ArrayList<String>): ArrayList<VetModel>{
        val newVets = ArrayList<VetModel>()
        for(vet in vets){
            var empaty = 0
            if(vet.sinergia[0] == tags.get(0)){ empaty +=1 }
            if(vet.sinergia[1] == tags.get(1)){ empaty +=1 }
            if(vet.sinergia[2] == tags.get(2)){ empaty +=1 }
            if(vet.sinergia[3] == tags.get(3)){ empaty +=1 }
            if(empaty >= 2) { newVets.add(vet)}
        }
        return newVets

    }

}