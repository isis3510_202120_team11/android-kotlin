package com.petyt.ui.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.*
import androidx.activity.viewModels
import com.petyt.R
import com.petyt.models.AppointModel
import com.petyt.utils.SharedPreferences
import com.petyt.models.WalkerModel
import com.petyt.utils.AppointLocal
import com.petyt.viewmodel.AppointViewModel
import com.petyt.viewmodel.PetViewModel
import com.petyt.viewmodel.WalkerViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AddApointActivity : AppCompatActivity() {

    lateinit var btnAgendar: Button
    lateinit var spPaseo: Spinner
    lateinit var spHoras: Spinner
    lateinit var spMinutos: Spinner
    lateinit var spHorario: Spinner
    lateinit var spDia: Spinner
    lateinit var spMes: Spinner
    lateinit var spYear: Spinner
    lateinit var spMascota: Spinner
    lateinit var etCosto: TextView

    val petViewModel: PetViewModel by viewModels()
    val walkerViewModel: WalkerViewModel by viewModels()
    val appointViewModel: AppointViewModel by viewModels()

    var walkerP = WalkerModel()
    var pet = ""
    var provider = ""
    var costo = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_apoint)

        petViewModel.onCreate(SharedPreferences(this).getOwner())//traer las mascotas del usiario

        appointViewModel.onCreate(this)// traer los datosde usuario
        walkerViewModel.onCreate() //obtener los datos del walke

        provider = intent.getStringExtra("walker").toString()
        btnAgendar = findViewById(R.id.btn_save_ap)
        spPaseo = findViewById(R.id.sp_paseo_ap)
        spHoras = findViewById(R.id.sp_hora_ap)
        spMinutos = findViewById(R.id.sp_minutos_ap)
        spHorario = findViewById(R.id.sp_time_ap)
        spMascota = findViewById(R.id.sp_mascotas_ap)
        spDia = findViewById(R.id.sp_dia_ap)
        spMes = findViewById(R.id.sp_mes_ap)
        spYear = findViewById(R.id.sp_year_ap)
        etCosto = findViewById(R.id.tv_coste_ap)
        spYear.lastVisiblePosition
        btnAgendar.setOnClickListener { saveAppoint() }
        btnAgendar.isEnabled = false

        petViewModel.petList.observe(this, { obtMascotas() })

        walkerViewModel.walkerData.observe(this, {
            walkerP = it
            val costo = "$ ${walkerP.price}"
            etCosto.text = costo
            setupHoras()
        })

        appointViewModel.results.observe(this, {
            if(it){
                Toast.makeText(this, "Fecha Agendada", Toast.LENGTH_SHORT).show()
                finish()
            }
            else{ Toast.makeText(this, "Error al agendar", Toast.LENGTH_SHORT).show() }
        })

        walkerViewModel.getWalker(provider)
    }
    fun saveAppoint(){
        val fecha = "${spDia.selectedItem}/${spMes.selectedItem}/${spYear.selectedItem}"
        val hora = "${spHoras.selectedItem}:${spMinutos.selectedItem} ${spHorario.selectedItem}"
        val time = spPaseo.selectedItem.toString()
        val client = SharedPreferences(this).getOwner()

        if(fecha.isEmpty()) return Toast.makeText(this, "Debes ingresar una fecha", Toast.LENGTH_SHORT).show()

        val ap = AppointModel(
            fecha = fecha, hora = hora, provider = walkerP.name, client = client,
            petName= pet, walkTime = time.toInt(), price = costo)

        appointViewModel.addApoint(ap)
    }
    private fun setupHoras(){
        spPaseo.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                costo = spPaseo.selectedItem.toString().toInt() * walkerP.price.toInt()
                val costo = "$ $costo"
                etCosto.text = costo
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
    }
    private fun obtMascotas(){
        val mascotas = ArrayList<String>()
        for(mas in petViewModel.petList.value!!){
            mascotas.add(mas.name)
        }
        if(mascotas.size == 0){
            Toast.makeText(this, "Debes agregar una mascota primero", Toast.LENGTH_SHORT).show()
            btnAgendar.isEnabled = false
            return
        }
        setupMascotas(mascotas)
    }
    private fun setupMascotas(mascotas: ArrayList<String>){
        spMascota.adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, mascotas)
        spMascota.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                pet = mascotas[position]
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(this@AddApointActivity, "Debes elegir una mascota primero", Toast.LENGTH_SHORT).show()
            }
        }
        pet = mascotas[0]
        btnAgendar.isEnabled = true
    }
}