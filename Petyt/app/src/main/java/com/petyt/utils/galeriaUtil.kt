package com.petyt.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import java.io.File

class galeriaUtil {
    fun seleccionarFoto(activity: Activity, codigo: Int){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        activity.startActivityForResult(intent, codigo)
    }

    fun guardarImagen(context: Context, id: Long, uri: Uri){
        val file = File(context.filesDir, id.toString())
        val bytes = context.contentResolver.openInputStream(uri)?.readBytes()!!
        file.writeBytes(bytes)
    }
    fun obtImageUri(context: Context, id: Long ): Uri {
        val file = File(context.filesDir, id.toString())
        return if (file.exists()) Uri.fromFile(file)
        else Uri.parse("android.resource://com.petyt/drawable/pets_boton")
    }
}