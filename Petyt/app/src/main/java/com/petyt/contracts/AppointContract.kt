package com.petyt.contracts

class AppointContract {
    companion object {
        val C_ID = "id"
        val N_TABLA = "Appointment"
        val C_FECHA = "fecha"
        val C_HORA = "hora"
        val C_PROVIDER = "provider"
        val C_CLIENT = "client"
        val C_PET = "petName"
        val C_PRICE = "price"
        val C_TIME = "walkTime"
    }
}