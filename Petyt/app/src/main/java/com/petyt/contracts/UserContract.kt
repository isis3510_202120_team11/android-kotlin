package com.petyt.contracts

class UserContract {
    companion object{
        val N_TABLA = "Users"
        val C_NAME = "name"
        val C_MAIL = "mail"
        val C_PASS = "password"
        val C_GENDER = "gender"
        val C_LOCATION = "location"
        val C_BIRTH = "birthDate"
        val C_IMG = "picture"
        val C_ID = "id"
    }
}