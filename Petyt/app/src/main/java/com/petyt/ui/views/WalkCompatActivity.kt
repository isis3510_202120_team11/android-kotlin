package com.petyt.ui.views

import android.content.Intent
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.petyt.R
import com.petyt.adapters.RvAdapterWalkers
import com.petyt.interfaces.ClickListenerWalker
import com.petyt.models.WalkerModel
import com.petyt.utils.LocationUtil
import com.petyt.utils.SharedPreferences
import com.petyt.utils.WalkersFilter
import com.petyt.viewmodel.PetViewModel
import com.petyt.viewmodel.UserViewModel
import com.petyt.viewmodel.WalkerViewModel

class WalkCompatActivity : AppCompatActivity() {

    lateinit var btnHome: Button
    lateinit var btnAgenda: Button

    lateinit var rvWalkers: RecyclerView
    lateinit var adapterLista: RvAdapterWalkers
    lateinit var layoutMan: RecyclerView.LayoutManager
    val walkerViewModel: WalkerViewModel by viewModels()
    val petViewModel: PetViewModel by viewModels()
    private lateinit var LocationUtil: LocationUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_walk_compat)
        walkerViewModel.onCreate()
        LocationUtil = LocationUtil(this)
        rvWalkers = findViewById(R.id.rv_compat_walk)
        btnHome = findViewById(R.id.btn_home_walkC)
        btnAgenda = findViewById(R.id.btn_agenda_walkC)
        rvWalkers.setHasFixedSize(true)
        layoutMan = LinearLayoutManager(this)
        btnHome.setOnClickListener { finish() }
        btnAgenda.setOnClickListener {
            startActivity(Intent(this, AgendaActivity::class.java))
            finish()
        }
        walkerViewModel.listWalkers.observe(this, { setupRecycler(it) })
        petViewModel.getPetList(SharedPreferences(this).getOwner())
        petViewModel.petList.observe(this, {
            if(it.size > 0) walkerViewModel.getWalkerList()
        })
    }

    fun setupRecycler(walkers: ArrayList<WalkerModel>){
        val pet = petViewModel.petList.value?.get(0)!!
        val tags = ArrayList<String>()
        with(tags){
            add(pet.breed)
            add(pet.age.toString())
            add(pet.size)
            add(pet.humor)
        }
        val filterWalkers = WalkersFilter().byTags(walkers, tags)
        adapterLista = RvAdapterWalkers(
            walkersList= filterWalkers,
            clickListen = object : ClickListenerWalker {
                override fun clickWalk(vista: View, index: Int) {
                    val intent = Intent(this@WalkCompatActivity, AddApointActivity::class.java)
                    intent.putExtra("walker", walkers[index].profile)
                    startActivity(intent)
                }
            })
        rvWalkers.layoutManager = layoutMan
        rvWalkers.adapter = adapterLista
    }
}