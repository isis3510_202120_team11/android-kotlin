package com.petyt.utils

import java.security.MessageDigest

object cifrado {
    fun encriptar(str: String): String{
        val message = str
        val md = MessageDigest.getInstance("MD5")
        val digest: ByteArray = md.digest(message.toByteArray())
        val stringBuilder = StringBuilder()
        digest.forEach {
            val value = it
            val hex = value.toInt() and (0xFF)
            val hexStr = Integer.toHexString(hex)
            if(hexStr.length == 1){ stringBuilder.append("0").append(hexStr)}
            else{ stringBuilder.append(hexStr) }
        }
        return stringBuilder.toString()
    }
}