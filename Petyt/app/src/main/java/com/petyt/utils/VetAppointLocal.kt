package com.petyt.utils

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.petyt.contracts.VetAppointContract

import com.petyt.db.DbVetHelper
import com.petyt.models.VetAppointModel

class VetAppointLocal(context: Context) {
    private var dbVetHelper: DbVetHelper? = null
    private val n_tabla = VetAppointContract.N_TABLA
    private val c_id = VetAppointContract.C_ID
    private val c_fecha = VetAppointContract.C_FECHA
    private val c_hora = VetAppointContract.C_HORA
    private val c_prov = VetAppointContract.C_PROVIDER
    private val c_client = VetAppointContract.C_CLIENT
    private val c_pet = VetAppointContract.C_PET
    private val c_price = VetAppointContract.C_PRICE

    init {
        dbVetHelper = DbVetHelper(context)
    }

    suspend fun  addVetAppoint(vetAppoint: VetAppointModel){
        val db: SQLiteDatabase = dbVetHelper?.writableDatabase!!
        val values = ContentValues().apply {
            put(c_fecha, vetAppoint.fecha)
            put(c_hora, vetAppoint.hora)
            put(c_prov, vetAppoint.provider)
            put(c_client, vetAppoint.client)
            put(c_pet, vetAppoint.petName)
            put(c_price, vetAppoint.price.toFloat())
        }
        val newRow = db.insert(n_tabla, null, values)
        db.close()
    }

    fun getVetAppoint(): ArrayList<VetAppointModel>{
        val items: ArrayList<VetAppointModel> = ArrayList()
        val db: SQLiteDatabase = dbVetHelper?.readableDatabase!!
        val columnas = arrayOf(c_fecha, c_hora, c_prov, c_client, c_pet, c_price)
        val c: Cursor = db.query(n_tabla, columnas, null, null, null, null, null)
        while (c.moveToNext()){
            items.add(VetAppointModel(
                fecha = c.getString(c.getColumnIndexOrThrow(c_fecha)),
                hora = c.getString(c.getColumnIndexOrThrow(c_hora)),
                provider = c.getString(c.getColumnIndexOrThrow(c_prov)),
                client = c.getString(c.getColumnIndexOrThrow(c_client)),
                petName = c.getString(c.getColumnIndexOrThrow(c_pet)),
                price = c.getFloat(c.getColumnIndexOrThrow(c_price)) as Number,
            ))
        }
        db.close()
        return items
    }
}