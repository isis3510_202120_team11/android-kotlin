package com.petyt.utils

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.GeoPoint

class SharedPreferences(var activity: AppCompatActivity) {
    val sharedPref = activity.getSharedPreferences("mail_user_preferences", Context.MODE_PRIVATE)
    private val userMail = "Mail"
    private val loc_latitud = "LTD"
    private val loc_longitude = "LGT"

    fun saveUserMail(mail:String){ //guardamos el email en la aplicacion para uso posterior
        with (sharedPref.edit()) {
            putString(userMail, mail)
            commit()
        }
    }
    fun saveLocation(latitude: Double, longitude: Double){
        with (sharedPref.edit()) {
            putString(loc_latitud, latitude.toString())
            putString(loc_longitude, longitude.toString())
            commit()
        }
    }
    fun getLocation(): GeoPoint{
        val latitud = sharedPref.getString(loc_latitud, "0.0")!!
        val longitud = sharedPref.getString(loc_longitude, "0.0")!!
        return GeoPoint(latitud.toDouble(), longitud.toDouble())
    }
    fun getOwner(): String{
        return sharedPref.getString(userMail, "")!!
    }
    fun clearCommit(){ sharedPref.edit().remove(userMail).apply() }
}