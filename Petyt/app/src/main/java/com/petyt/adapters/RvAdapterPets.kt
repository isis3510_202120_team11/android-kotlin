package com.petyt.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.petyt.R
import com.petyt.utils.Base64Converter
import com.petyt.interfaces.ClickListenerPet
import com.petyt.models.PetModel

class RvAdapterPets (petsList:ArrayList<PetModel>, var clickListen: ClickListenerPet): RecyclerView.Adapter<RvAdapterPets.viewHold>() {

    var pets: ArrayList<PetModel> = petsList
    lateinit var viewHolder: viewHold

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHold {
        val vista = LayoutInflater.from(parent.context).inflate(R.layout.pet_view, parent, false)
        viewHolder = viewHold(vista, clickListen)
        return viewHolder
    }

    override fun onBindViewHolder(holder: viewHold, position: Int) {
        val pet = pets[position]
        val converter = Base64Converter()
        holder.name.text = pet.name
        holder.age.text = pet.age.toString()
        holder.breed.text = pet.breed
        holder.sex.text = pet.sex
        if(pet.imageStr.isEmpty()){
            holder.photo.setImageResource(R.drawable.pets_boton)
        }else{
            holder.photo.setImageBitmap(converter.getBitmapImage(pet.imageStr))
        }
    }

    override fun getItemCount(): Int {
        return pets.size
    }

    class viewHold(var vista: View, listen: ClickListenerPet): RecyclerView.ViewHolder(vista), View.OnClickListener{
        var name: TextView
        var age: TextView
        var sex: TextView
        var breed: TextView
        var photo: ImageView
        var listener: ClickListenerPet

        init {
            this.listener = listen
            name = vista.findViewById(R.id.tv_name_pet)
            age = vista.findViewById(R.id.tv_edad_pet)
            sex = vista.findViewById(R.id.tv_sex_pet)
            breed = vista.findViewById(R.id.tv_raza_pet)
            photo = vista.findViewById(R.id.iv_pet_photo)
            vista.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            this.listener.clickPet(v!!, adapterPosition)
        }
    }
}