package com.petyt.ui.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.petyt.R
import com.petyt.adapters.RvAdapterWalkers
import com.petyt.interfaces.ClickListenerWalker
import com.petyt.models.WalkerModel
import com.petyt.viewmodel.WalkerViewModel

class BestWalkersActivity : AppCompatActivity() {

    lateinit var btnHome: Button
    lateinit var btnAgenda: Button

    lateinit var rvWalkers: RecyclerView
    lateinit var adapterLista: RvAdapterWalkers
    lateinit var layoutMan: RecyclerView.LayoutManager
    val walkerViewModel: WalkerViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_best_walkers)
        walkerViewModel.onCreate()
        rvWalkers = findViewById(R.id.rv_best_walkers)
        rvWalkers.setHasFixedSize(true)
        layoutMan = LinearLayoutManager(this)
        btnHome = findViewById(R.id.btn_home_walk_recom)
        btnAgenda = findViewById(R.id.btn_agenda_walk_recom)
        btnHome.setOnClickListener { finish() }
        btnAgenda.setOnClickListener { finish() }
        walkerViewModel.listWalkers.observe(this, { setupRecycler(it) })
        walkerViewModel.obtBestWalkers()
    }

    fun setupRecycler(walkers: ArrayList<WalkerModel>){
        adapterLista = RvAdapterWalkers(
            walkersList= walkers,
            clickListen = object : ClickListenerWalker {
                override fun clickWalk(vista: View, index: Int) {

                }
            })
        rvWalkers.layoutManager = layoutMan
        rvWalkers.adapter = adapterLista
    }
}