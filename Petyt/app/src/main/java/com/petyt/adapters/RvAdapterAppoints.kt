package com.petyt.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.petyt.R
import com.petyt.interfaces.ClickListenerPet
import com.petyt.models.AppointModel

class RvAdapterAppoints(walkersList:ArrayList<AppointModel>, var clickListen: ClickListenerPet): RecyclerView.Adapter<RvAdapterAppoints.viewHold>() {

    var walkers: ArrayList<AppointModel> = walkersList
    lateinit var viewHolder: viewHold

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHold {
        val vista = LayoutInflater.from(parent.context).inflate(R.layout.appoints_view, parent, false)
        viewHolder = viewHold(vista, clickListen)
        return viewHolder
    }

    override fun onBindViewHolder(holder: viewHold, position: Int) {
        val appoint = walkers[position]
        holder.fecha.text = appoint.fecha
        holder.hora.text = appoint.hora
        holder.price.text = appoint.price.toString()
        holder.walker.text = appoint.provider
        val paseo = "Paseo de ${appoint.walkTime} horas"
        holder.time.text = paseo
    }

    override fun getItemCount(): Int {
        return walkers.size
    }

    class viewHold(var vista: View, listen: ClickListenerPet): RecyclerView.ViewHolder(vista), View.OnClickListener{
        var fecha: TextView
        var hora: TextView
        var time: TextView
        var price: TextView
        var walker: TextView
        var listener: ClickListenerPet

        init {
            this.listener = listen
            fecha = vista.findViewById(R.id.tv_ap_fecha)
            hora = vista.findViewById(R.id.tv_ap_hora)
            time = vista.findViewById(R.id.tv_ap_time)
            price = vista.findViewById(R.id.tv_costo_ap)
            walker = vista.findViewById(R.id.tv_paseador_ap)
            vista.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            this.listener.clickPet(v!!, adapterPosition)
        }
    }
}