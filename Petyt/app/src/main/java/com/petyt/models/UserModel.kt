package com.petyt.models

import com.google.firebase.firestore.GeoPoint


data class UserModel(
    val name: String = "",
    val mail: String = "",
    val password: String = "",
    val gender: String = "",
    val birth: String = "",
    val image: String = "",
    val location: GeoPoint = GeoPoint(0.0, 0.0)
)

