package com.petyt.ui.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.petyt.R
import com.petyt.utils.SharedPreferences
import com.petyt.utils.cifrado

class LoginActivity : AppCompatActivity() {
    lateinit var etMail: EditText
    lateinit var etPass: EditText
    lateinit var btnLogin: Button
    lateinit var btnRegister: Button
    lateinit var shared: SharedPreferences
    private val auth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        if (SharedPreferences(this).getOwner() != "") goServices()
        btnLogin = findViewById(R.id.btn_iniciar_sesion) //definir el elemento
        etMail = findViewById(R.id.et_mail_login)
        etPass = findViewById(R.id.et_pass_login)
        btnRegister = findViewById(R.id.btn_register)
        shared = SharedPreferences(this)
        btnRegister.setOnClickListener { goRegister() }
        btnLogin.setOnClickListener { iniciarSesion() }
    }

    fun iniciarSesion(){
        val mail = etMail.text.toString()
        val pass = cifrado.encriptar(etPass.text.toString())
        Log.d("cypher", pass)
        if(mail.isNotEmpty() && pass.isNotEmpty()){
            auth.signInWithEmailAndPassword(mail, pass).addOnCompleteListener { user ->
                if (!user.isSuccessful){ Toast.makeText(this, "usuario y/o password incorrecto", Toast.LENGTH_LONG).show() }
                else {
                    shared.saveUserMail(mail)
                    goServices()
                }

            }
        }
    }
    fun goServices(){
        startActivity(Intent(this, ServicesActivity::class.java)) //vamos a servicios
        finish()
    }
    fun goRegister(){
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }
}
//FireBaseAuth.getInstance().signOut()